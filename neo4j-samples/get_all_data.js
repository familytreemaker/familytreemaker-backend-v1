async function getAllData(session) {
  try {
    const data = {};
    let query = `MATCH(n) RETURN (n)`;
    const result = await session.run(query);
    for (let record of result.records) {
      const node = record.get(0).properties;
      const personID = node["personID"];
      const personName = node["name"];

      // get children
      query = `match (a) -[r:PARENT_OF]-> (b) WHERE a.personID="${personID}" return b`;
      const childrenResult = await session.run(query);
      const children = [];
      for (let childrenRecord of childrenResult.records) {
        const r = childrenRecord.get(0).properties;
        children.push(r["personID"]);
      }

      // get parents
      query = `match (a) -[r:CHILD_OF]-> (b) WHERE a.personID="${personID}" return b`;
      const parentResult = await session.run(query);
      const parents = [];
      for (let parentRecord of parentResult.records) {
        const r = parentRecord.get(0).properties;
        parents.push(r["personID"]);
      }

      // get spouse
      query = `match (a) -[r:SPOUSE_OF]- (b) WHERE a.personID="${personID}" return b`;
      const spouseResult = await session.run(query);
      let spouse = "";
      if (spouseResult.records && spouseResult.records.length > 0) {
        spouse = spouseResult.records[0].get(0).properties["personID"];
      }

      data[personID] = {
        id: personID,
        name: personName,
        children: children,
        parents: parents,
        spouse: spouse,
      };
    }
    console.log(JSON.stringify(data, null, 2));
  } finally {
    await session.close();
  }

  // on application exit:
  await driver.close();
}

exports.getAllData = getAllData;
