const express = require("express");
const admin = require("firebase-admin");

const fs = require("fs");
const { addRelationship, getAllData, addMember } = require("./familyTreeUtils");
const app = express();
const port = 5000;
const neo4j = require("neo4j-driver");

const user = process.env.NEO4J_USER;
const password = process.env.NEO4J_PASSWORD;
const uri = process.env.NEO4J_URI;
const db = process.env.NEO4J_DB;
const serviceAccountFile = "../config/adminconfig.json";

let data = {};

var serviceAccount = require(serviceAccountFile);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

async function waitUntilNeo4JIsReady() {
  const retries = 10;
  const delayInterval = 3;
  const delay = async (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  for (let trial = 0; trial < retries; trial++) {
    try {
      let d = neo4j.driver(uri, neo4j.auth.basic(user, password));
      let s = d.session({
        database: db,
      });
      let query = `MATCH(n) RETURN (n) LIMIT 1`;
      const result = await s.run(query);
      await s.close();
      await d.close();
      return result;
    } catch (ex) {
      await delay(1000 * delayInterval);
    }
  }
}

waitUntilNeo4JIsReady()
  .then(async () => {
    const driver = neo4j.driver(uri, neo4j.auth.basic(user, password));
    const session = driver.session({
      database: db,
    });
    data = await getAllData(session);
    app.listen(port, () => {
      console.log(`Family tree utils app started`);
    });
  })
  .catch((ex) => {
    process.exit(1);
  });

// parse application/json
app.use(express.json());

app.use(decodeIDToken);

function isAuthenticated(req) {
  const user = req["currentUser"];
  if (!user) {
    return false;
  } else {
    return true;
  }
}

async function handleRequest(req, res, protectedFunction) {
  if (!isAuthenticated(req)) {
    res.status(403).send("Unauthorized!");
  } else {
    await protectedFunction(req, res);
  }
}

async function decodeIDToken(req, res, next) {
  if (req.headers?.authorization?.startsWith("Bearer ")) {
    const idToken = req.headers.authorization.split("Bearer ")[1];
    try {
      const decodedToken = await admin.auth().verifyIdToken(idToken);
      req["currentUser"] = decodedToken;
    } catch (err) {
      console.log(err);
    }
  }

  next();
}

async function apiGetAll(req, res) {
  res.json(data);
  return new Promise((ok, err) => {});
}

async function apiCreate(req, res) {
  try {
    const result = await addMember(req.body);
    const body = result;
    const id = body["id"];
    data[id] = body;
    res.json(req.body);
  } catch (ex) {
    res.json({
      status: "Error",
    });
  }
}

async function apiCreateRelationship(req, res) {
  try {
    const result = await addRelationship(data, req.body);
    res.json(result);
  } catch (ex) {
    console.log("Error", ex);
    res.json({
      status: "Error when creating relationship",
    });
  }
}

async function apiSave(req, res) {
  fs.writeFile("./tmp.json", JSON.stringify(data), (err) => {
    if (err) {
      res.json({ status: "error" });
    } else {
      res.json({ status: "OK" });
    }
  });
  return new Promise((ok, err) => {});
}

async function apiGetUser(req, res) {
  res.json({
    name: req.params,
  });
  return new Promise((ok, err) => {});
}

app.get("/api/getAll", (req, res) => {
  handleRequest(req, res, apiGetAll);
});

app.post("/api/create", async function (req, res) {
  handleRequest(req, res, apiCreate);
});

app.post("/api/create-relationship", async function (req, res) {
  handleRequest(req, res, apiCreateRelationship);
});

app.post("/api/save", function (req, res) {
  handleRequest(req, res, apiSave);
});

app.get("/api/get/:userId", function (req, res) {
  handleRequest(req, res, apiGetUser);
});
