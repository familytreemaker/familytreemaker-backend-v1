const neo4j = require("neo4j-driver");
const { FLAGS } = require("./featureFlags");

const user = process.env.NEO4J_USER;
const password = process.env.NEO4J_PASSWORD;
const uri = process.env.NEO4J_URI;
const db = process.env.NEO4J_DB;

const driver = neo4j.driver(uri, neo4j.auth.basic(user, password));
const session = driver.session({
  database: db,
});

function parentOf(data, relationshipObject) {
  let source = relationshipObject[0];
  let target = relationshipObject[2];
  let parentID = source["id"];
  let childID = target["id"];

  addParent(data, childID, parentID);
  addChild(data, parentID, childID);

  if (FLAGS.APPEND_CHILDREN_WHEN_ADDING_SPOUSE) {
    let spouseID = data[parentID]["spouse"];
    addParent(data, childID, spouseID);
    addChild(data, spouseID, childID);
  }
}

async function parentOf_db(data, relationshipObject) {
  let source = relationshipObject[0];
  let target = relationshipObject[2];
  let parentID = source["id"];
  let childID = target["id"];

  const personID = parentID.replace(/-/g, "_");
  childID = childID.replace(/-/g, "_");
  let query = `MATCH(a), (b) WHERE a.personID="${personID}" AND b.personID="${childID}" MERGE (a)-[:PARENT_OF]->(b)-[:CHILD_OF]->(a) RETURN a,b`;
  await session.run(query);

  if (FLAGS.APPEND_CHILDREN_WHEN_ADDING_SPOUSE) {
    // #TODO: spouseID should be obtained from database instead of data object
    let spouseID = data[parentID]["spouse"].replace(/-/g, "_");
    query = `MATCH(a), (b) WHERE a.personID="${spouseID}" AND b.personID="${childID}" MERGE (a)-[:PARENT_OF]->(b)-[:CHILD_OF]->(a) RETURN a,b`;
    await session.run(query);
  }
  return relationshipObject;
}

function spouseOf(data, relationshipObject) {
  let a = relationshipObject[0];
  let b = relationshipObject[2];

  let aID = a["id"];
  let bID = b["id"];
  data[aID]["spouse"] = bID;
  data[bID]["spouse"] = aID;

  if (FLAGS.APPEND_CHILDREN_WHEN_ADDING_SPOUSE) {
    let aChildren = data[aID]["children"];
    let bChildren = data[bID]["children"];
    let allChildren = bChildren.concat(aChildren);
    for (let child of allChildren) {
      addParent(data, child, aID);
      addParent(data, child, bID);
      addChild(data, aID, child);
      addChild(data, bID, child);
    }
  }
}

async function spouseOf_db(data, relationshipObject) {
  let a = relationshipObject[0];
  let b = relationshipObject[2];

  let aID = a["id"];
  let bID = b["id"];

  const personID = aID.replace(/-/g, "_");
  const spouseID = bID.replace(/-/g, "_");
  const query = `MATCH(a), (b) WHERE a.personID="${personID}" AND b.personID="${spouseID}" MERGE (a)-[:SPOUSE_OF]-(b) RETURN a,b`;
  await session.run(query);

  if (FLAGS.APPEND_CHILDREN_WHEN_ADDING_SPOUSE) {
    let aChildren = data[aID]["children"];
    let bChildren = data[bID]["children"];
    let allChildren = bChildren.concat(aChildren);
    for (let child of allChildren) {
      let childID = child.replace(/-/g, "_");
      let query = `MATCH(a), (b) WHERE a.personID="${personID}" AND b.personID="${childID}" MERGE (a)-[:PARENT_OF]->(b)-[:CHILD_OF]->(a) RETURN a,b`;
      await session.run(query);
      query = `MATCH(a), (b) WHERE a.personID="${spouseID}" AND b.personID="${childID}" MERGE (a)-[:PARENT_OF]->(b)-[:CHILD_OF]->(a) RETURN a,b`;
      await session.run(query);
    }
  }
}

function childOf(data, relationshipObject) {
  let child = relationshipObject[0];
  let parent = relationshipObject[2];

  let childID = child["id"];
  let parentID = parent["id"];

  addChild(data, parentID, childID);
  addParent(data, childID, parentID);

  if (FLAGS.APPEND_CHILDREN_WHEN_ADDING_SPOUSE) {
    let spouseID = data[parentID]["spouse"];
    addChild(data, spouseID, childID);
    addParent(data, childID, spouseID);
  }
}

async function childOf_db(data, relationshipObject) {
  let child = relationshipObject[0];
  let parent = relationshipObject[2];

  let childID = child["id"];
  let parentID = parent["id"];

  const personID = parentID.replace(/-/g, "_");
  childID = childID.replace(/-/g, "_");
  let query = `MATCH(a), (b) WHERE a.personID="${personID}" AND b.personID="${childID}" MERGE (a)-[:PARENT_OF]->(b)-[:CHILD_OF]->(a) RETURN a,b`;
  await session.run(query);

  if (FLAGS.APPEND_CHILDREN_WHEN_ADDING_SPOUSE) {
    // #TODO: spouseID should be obtained from database instead of data object
    let spouseID = data[parentID]["spouse"].replace(/-/g, "_");
    query = `MATCH(a), (b) WHERE a.personID="${spouseID}" AND b.personID="${childID}" MERGE (a)-[:PARENT_OF]->(b)-[:CHILD_OF]->(a) RETURN a,b`;
    await session.run(query);
  }
  return relationshipObject;
}

function addChild(data, parentID, childID) {
  let children = data[parentID]["children"];
  children.push(childID);
  children = Array.from(new Set(children));
  data[parentID]["children"] = children;
}

function addParent(data, childID, parentID) {
  let parents = data[childID]["parents"];
  parents.push(parentID);
  parents = Array.from(new Set(parents));
  data[childID]["parents"] = parents;
}

async function addRelationship(data, relationshipObject) {
  let type = relationshipObject[1];
  let typeID = type["id"];

  if (typeID === 0) {
    parentOf(data, relationshipObject);
    await parentOf_db(data, relationshipObject);
  } else if (typeID === 1) {
    spouseOf(data, relationshipObject);
    await spouseOf_db(data, relationshipObject);
  } else if (typeID === 2) {
    childOf(data, relationshipObject);
    await childOf_db(data, relationshipObject);
  }
  return relationshipObject;
}

async function addMember(body) {
  const personID = body["id"].replace(/-/g, "_");
  const personName = body["name"];
  const primaryKey = personName.split(" ")[0].split(",")[0].split(".")[0];
  const query = `CREATE (${primaryKey}:Person {name: $name, personID: $id})`;
  await session.run(query, {
    name: personName,
    id: personID,
  });
  return body;
}

async function getAllData() {
  const data = {};
  let query = `MATCH(n) RETURN (n)`;
  const result = await session.run(query);
  for (let record of result.records) {
    const node = record.get(0).properties;
    const personID = node["personID"];
    const personName = node["name"];

    // get children
    query = `match (a) -[r:PARENT_OF]-> (b) WHERE a.personID="${personID}" return b`;
    const childrenResult = await session.run(query);
    const children = [];
    for (let childrenRecord of childrenResult.records) {
      const r = childrenRecord.get(0).properties;
      children.push(r["personID"]);
    }

    // get parents
    query = `match (a) -[r:CHILD_OF]-> (b) WHERE a.personID="${personID}" return b`;
    const parentResult = await session.run(query);
    const parents = [];
    for (let parentRecord of parentResult.records) {
      const r = parentRecord.get(0).properties;
      parents.push(r["personID"]);
    }

    // get spouse
    query = `match (a) -[r:SPOUSE_OF]- (b) WHERE a.personID="${personID}" return b`;
    const spouseResult = await session.run(query);
    let spouse = "";
    if (spouseResult.records && spouseResult.records.length > 0) {
      spouse = spouseResult.records[0].get(0).properties["personID"];
    }

    data[personID] = {
      id: personID,
      name: personName,
      children: children,
      parents: parents,
      spouse: spouse,
    };
  }
  return data;
}

exports.getAllData = getAllData;
exports.addRelationship = addRelationship;
exports.addMember = addMember;
