FROM node:14-alpine3.12

# set working directory
WORKDIR /

# install app dependencies
COPY package.json ./
RUN npm install --silent

# add app
COPY . ./

# start app
CMD ["npm", "start"]
